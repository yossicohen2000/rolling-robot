﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    // Use this for initialization
    public float speedrot = 150;
    public float speedmov = 3, JumpForce = 5;
    private Rigidbody rb;
    float x;
    float z;

    public float SuperG = 20;
float angleDiff;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }
    void Update()
    {
        if (Input.GetButtonDown("Jump")) rb.AddForce(JumpForce * Vector3.up, ForceMode.VelocityChange);
    }
    // Update is called once per frame
    void FixedUpdate()
    {
		angleDiff=-Vector3.Angle(Vector3.ProjectOnPlane(Camera.main.transform.forward,Vector3.up),transform.forward)*(Vector3.Cross(Vector3.ProjectOnPlane(Camera.main.transform.forward,Vector3.up),transform.forward).y);
        x = Input.GetAxis("Horizontal") * speedrot;
        z = Input.GetAxis("Vertical") * speedmov;
        GetComponentInChildren<Animator>().speed = transform.InverseTransformVector(rb.velocity).z;
        rb.AddForce(new Vector3(0, -SuperG, 0), ForceMode.VelocityChange);
        rb.AddRelativeTorque(new Vector3(0, - rb.angularVelocity.y + angleDiff, 0), ForceMode.VelocityChange);
        rb.AddForce(new Vector3(x, 0, z), ForceMode.VelocityChange);
        // transform.Rotate(0, x, 0);
        // transform.Translate(0, 0, z);
    }
}
