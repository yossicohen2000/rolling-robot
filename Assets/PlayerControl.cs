﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour {

    public float movespeed = 3.0f;
    public float rotationspeed = 150.0f;

	void Start () {
		
	}

    // Update is called once per frame
    void Update()
    {
        var x = Input.GetAxis("Horizontal") * Time.deltaTime * rotationspeed;
        var z = Input.GetAxis("Vertical") * Time.deltaTime * movespeed;

        if(Input.GetAxis("Vertical") > 0)
        {
            Animator anima = GetComponent<Animator>();
            anima.SetBool("Rolling on", true);
        }
        else
        {
            Animator anima = GetComponent<Animator>();
            anima.SetBool("Rolling on", false);
        }
        transform.Rotate(0, x, 0);
        transform.Translate(0, 0, z);
    }
}
