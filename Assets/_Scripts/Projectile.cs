﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public int Damage = 1;
    public float ProjectileSpeed = 5,CritChance=0.1f, MaxCritScale = 2.5f;
    public string[] TagTargets = { "Enemy" };
    Rigidbody rb;
    Transform myref;
    public bool DieOut = false, MaintainVelocity = false,DestroyOnHit=true;
    public float DieoutTime = 10;
    Vector3 dir;
	public GameObject HitEffect;
    float timeOfShot, timeToDie;

    // Use this for initialization
    void Start()
    {
        myref = transform;
        timeOfShot = Time.time;
        timeToDie = timeOfShot + DieoutTime;
        rb = GetComponent<Rigidbody>();

        dir = myref.forward;
        rb.velocity = (dir * ProjectileSpeed);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
		rb.AddForce(Vector3.up*6,ForceMode.Acceleration);
        if (MaintainVelocity)
            rb.velocity = (dir * ProjectileSpeed);
		myref.LookAt(rb.velocity);
        if (DieOut && Time.time > timeToDie)
            Destroy(this.gameObject); //Destroying
    }


	public void DestroyProjectile()
    {
        if (HitEffect) Instantiate(HitEffect, myref.position, myref.rotation);
        Destroy(gameObject, 0.024f);
    }
	private void OnCollisionEnter(Collision other) {
		        foreach (string s in TagTargets)
            if (other.gameObject.tag == s)
            {
                
                if (Random.value < CritChance) other.gameObject.GetComponent<HealthManager>().hit(Mathf.CeilToInt(Damage * Random.Range(1.1f, MaxCritScale)));
                else other.gameObject.GetComponent<HealthManager>().hit(Damage);
                
            }
            if (DestroyOnHit)
                    DestroyProjectile();
	}
}
