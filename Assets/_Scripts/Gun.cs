﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour {
public GameObject Bullet,Effect;
    Transform myref;
float TimeToAttack=0;
public int dmg=1;
public AudioClip SFX;
public float crit=0.1f;
public float fireRate = 3;

	// Use this for initialization
	void Start () {
		myref = transform;
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetButton("Fire1"))attack();
	}
	void attack()
    {
        if (TimeToAttack < Time.time)
        {
            SoundManager.instance.PlaySingle(SFX);
            GameObject proj=Instantiate(Bullet,myref.position,myref.rotation) as GameObject;
            if(Effect)Instantiate(Effect,myref.position,myref.rotation);
			
            TimeToAttack = Time.time + 1 / fireRate;
            GetComponent<Animator>().SetTrigger("shoot");
        }
        // Debug.Log("called");
    }

}
