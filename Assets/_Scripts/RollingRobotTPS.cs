﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RollingRobotTPS : MonoBehaviour
{
    public float speed = 2,GrabityMult=10,GroundCheckRadiu=0.45f,AirAccelerationFactor=0.2f;
    public Vector3 force, dir, dist;
    Rigidbody rb;
    Transform MyTrans, BodyTrans, AxisTrans, WheelTrans,LeftArm;
    public Transform CamTrans, Target;
    Vector3 localVel;
	public LayerMask WhatIsGround;
    public bool Grounded;
    Vector2 move = Vector2.zero;
    private Vector3 vel;
    float rotvel = 0;

    public float RotationSpeed=5;

    // Use this for initialization
    void Start()
    {
        MyTrans = transform;
        rb = GetComponent<Rigidbody>();
        BodyTrans = MyTrans.Find("Body Holder");
        AxisTrans = MyTrans.Find("Ball Axis");
        WheelTrans = AxisTrans.Find("Rolling-Sphere");
        LeftArm = BodyTrans.Find("Left Arm");
        CamTrans = Camera.main.transform;
    }

Vector3 axisVel;
    void FixedUpdate()
    {
		
        Grounded = Physics.CheckSphere(WheelTrans.position, GroundCheckRadiu,WhatIsGround);
        move = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));

        force = new Vector3(move.x * speed, 0, move.y * speed);
        force = Grounded ? force : force * AirAccelerationFactor;
        rb.AddRelativeForce(force-Vector3.up*GrabityMult,ForceMode.Acceleration);

        vel = rb.velocity;
        localVel = MyTrans.InverseTransformVector(vel);
        BodyTrans.localEulerAngles = new Vector3(localVel.z, 0, -localVel.x)*1.5f; //leaning
        LeftArm.localEulerAngles = new Vector3(localVel.z, 0, -localVel.x)*1; //leaning

        axisVel+=0.2f*(localVel-axisVel);
        var ballRotTarget=Quaternion.Euler(0, Mathf.Atan2(axisVel.x, axisVel.z) * Mathf.Rad2Deg, 0);
        if(Grounded&&rotvel>1f)AxisTrans.localRotation = Quaternion.Slerp(AxisTrans.localRotation,ballRotTarget,0.1f);
        else if(rotvel>0.3f) AxisTrans.localRotation = Quaternion.Slerp(AxisTrans.localRotation,ballRotTarget,0.1f*rotvel);
        else AxisTrans.localRotation = Quaternion.Slerp(AxisTrans.localRotation,Quaternion.identity,0.05f);

        if (Grounded) rotvel += Time.fixedDeltaTime * 10f * (vel.magnitude - rotvel);
        else rotvel -= rotvel * Time.fixedDeltaTime * 0.5f;
        WheelTrans.Rotate(rotvel, 0, 0);

        dir = CamTrans.forward;
        float angleDiff = Vector3.Angle(MyTrans.forward, Vector3.Scale(dir.normalized, new Vector3(1, 0, 1)));

        angleDiff = Vector3.Cross(MyTrans.forward, Vector3.Scale(dir.normalized, new Vector3(1, 0, 1))).y > 0 ? angleDiff : -angleDiff;
        MyTrans.Rotate(0, angleDiff * Time.fixedDeltaTime * RotationSpeed, 0);
        rb.angularVelocity = Vector3.zero;
    }
}
