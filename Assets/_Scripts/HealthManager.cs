﻿using UnityEngine;
using System.Collections;
public class HealthManager : MonoBehaviour
{

    [HeaderAttribute("Basic")]
    public float StartingHealth = 10;
    public float hp = 10;
    public bool player = false;

    public bool dontKill = false;
    [SpaceAttribute(10)]
    [HeaderAttribute("Behavior")]
    public float invulnerabletime = 0;
    public bool SlowDownOnHit = false;
    public float knockback = 5;
    public bool DirectionalKnockback = false;
    [SpaceAttribute(10)]
    [HeaderAttribute("Objects")]
    public float HealthBarInitialSize = 2;
    public float VerticalOffset = -3;
    public float ExplosionRange = 10;
    public float ExplosionPower = 2;
    public float ExplosionChance = 0;

    public bool HitText = false;
    public Color HitTextColor = new Color(1, 0.1f, 0.1f);

    public bool SetTint = false;

    public GameObject HealthBar, explode, explosion, FlyingHitText, Matter;

    Transform myref;
    private float hitTimestamp = -1;



    bool altexplode = false,invulnerable=false;


    // Use this for initialization
    void Start()
    {
        myref = transform;
        StartingHealth = hp;
        if (HealthBar)
        {
            HealthBar = Instantiate(HealthBar, myref.position + VerticalOffset * Vector3.up, Quaternion.identity) as GameObject;
            HealthBar.transform.localScale = new Vector3(HealthBarInitialSize, HealthBar.transform.localScale.y, HealthBar.transform.localScale.z);
        }
    }


    // Update is called once per frame
    void FixedUpdate()
    {
        invulnerable=Time.time < hitTimestamp + invulnerabletime;
        if (HealthBar)
        {
            HealthBar.transform.position = myref.position + VerticalOffset * Vector3.up - 0.5f * HealthBarInitialSize * Vector3.right;
        }
        if (hp <= 0 && !dontKill)
        {
            kill();
        }
        if (player) playerOnly();
    }

    private void playerOnly()
    {
        {
            if (invulnerable)
            {
                GetComponentInChildren<MeshRenderer>().enabled = !GetComponentInChildren<MeshRenderer>().enabled;
                if (SlowDownOnHit) { Time.timeScale = 0.75f; }
                // Physics2D.IgnoreLayerCollision(gameObject.layer, 11, true);
            }
            else
            {
                // Physics2D.IgnoreLayerCollision(gameObject.layer, 11, false);
                GetComponentInChildren<MeshRenderer>().enabled = true;
                if (SlowDownOnHit) Time.timeScale = 1;
            }
        }
    }


    public void hit(float dmg)
    {
        if (dmg > 0)
        {
            if (HealthBar) HealthBar.transform.localScale = new Vector3(((float)hp / StartingHealth) * HealthBarInitialSize, HealthBar.transform.localScale.y, HealthBar.transform.localScale.z);
            if (!invulnerable)
            {
                hp -= dmg;
                hitTimestamp = Time.time;
                if (HitText)
                {
                    GameObject HTxt = Instantiate(FlyingHitText, myref.position, Quaternion.identity) as GameObject;
                    HTxt.GetComponent<TextMesh>().text = dmg.ToString();
                    HTxt.GetComponent<TextMesh>().color = HitTextColor;
                }
            }
        }

    }
    public void hitFrom(float dmg, Vector3 loc, float KnockbackMult)
    {
        if (dmg > 0)
        {
            if(!invulnerable)
            {
                hp -= dmg;
                hitTimestamp = Time.time;


                if (HitText)
                {
                    GameObject HTxt = Instantiate(FlyingHitText, myref.position, Quaternion.identity) as GameObject;
                    HTxt.GetComponent<TextMesh>().text = dmg.ToString();
                    HTxt.GetComponent<TextMesh>().color = HitTextColor;
                }
            }
            if (knockback > 0)
            {
    
            }
        }
    }


    public void kill()
    {
        if (Random.value < ExplosionChance)
        {
            altexplode = true;
        }

        if (!altexplode) Instantiate(explode, myref.position, Quaternion.identity);
        Destroy(gameObject);
    }
    public void SetHP(int health)
    {
        if (health > StartingHealth) StartingHealth = health;
        hp = health;
    }
    IEnumerator Flash()
    {
        //enemy Flash effect here
        yield return new WaitForSeconds(0.1f);
        //enemy Flash effect here
    }



}