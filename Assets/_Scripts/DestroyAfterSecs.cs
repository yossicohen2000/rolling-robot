﻿using UnityEngine;
using System.Collections;

public class DestroyAfterSecs : MonoBehaviour
{
    public bool Sound = true;
    public float DestroyTime = 5;
    void Start()
    {
        if (Sound)
        {
            GetComponent<AudioSource>().pitch = Time.timeScale * (1 + Random.Range(-0.2f, 0.2f));
            GetComponent<AudioSource>().Play();
        }
        Destroy(gameObject, DestroyTime);
    }


}