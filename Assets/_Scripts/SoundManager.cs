﻿using UnityEngine;
using System.Collections;


public class SoundManager : MonoBehaviour
{
    public static SoundManager instance = null;     //Allows other scripts to call functions from SoundManager.
    
    public AudioSource musicSource;  
    public AudioSource sfxSource, sfxSource2,RandomSfxSource,SpecialSfxSource, SpecialSfxSource2;                   //Drag a reference to the audio source which will play the sound effects.               //Drag a reference to the audio source which will play the music.             
    public float lowPitchRange = .95f;              //The lowest a sound effect will be randomly pitched.
    public float highPitchRange = 1.05f;            //The highest a sound effect will be randomly pitched.
    public bool SlowmoMusic = true;
    public bool SlowmoSfx = true;
    public float scale = 1;
    public bool debugtime = false;
    bool FirstSource = true;
    bool FirstSpSource = true;
    void Awake()
    {
        //Check if there is already an instance of SoundManager
        if (instance == null)
            //if not, set it to this.
            instance = this;
        //If instance already exists:
        else if (instance != this)
            //Destroy this, this enforces our singleton pattern so there can only be one instance of SoundManager.
            Destroy(gameObject);
        //Set SoundManager to DontDestroyOnLoad so that it won't be destroyed when reloading our scene.
    }


    //Used to play single sound clips.
    public void PlaySingle(AudioClip clip)
    {
        //Set the clip of our efxSource audio source to the clip passed in as a parameter.
        sfxSource.clip = clip;
        if (SlowmoSfx) randomPitch = Random.Range(lowPitchRange, highPitchRange) * Time.timeScale;
        else randomPitch = Random.Range(lowPitchRange, highPitchRange);
        if (FirstSource)
        {
            sfxSource.clip = clip;//Set the pitch of the audio source to the randomly chosen pitch.
            sfxSource.pitch = randomPitch;
            //Play the clip.
            sfxSource.Play();
            FirstSource = false;
        }
        else
        {
            sfxSource2.clip = clip;
            sfxSource2.pitch = randomPitch;
            //Play the clip.
            sfxSource2.Play();
            FirstSource = true;
        }
    }
    public void PlaySpecial(AudioClip clip)
    {
        //Set the clip of our efxSource audio source to the clip passed in as a parameter.
        SpecialSfxSource.clip = clip;
        if (SlowmoSfx) randomPitch = Random.Range(lowPitchRange, highPitchRange) * Time.timeScale;
        else randomPitch = Random.Range(lowPitchRange, highPitchRange);
        if (FirstSpSource)
        {
            SpecialSfxSource.clip = clip;//Set the pitch of the audio source to the randomly chosen pitch.
            SpecialSfxSource.pitch = randomPitch;
            //Play the clip.
            SpecialSfxSource.Play();
            FirstSpSource = false;
        }
        else
        {
            SpecialSfxSource2.clip = clip;
            SpecialSfxSource2.pitch = randomPitch;
            //Play the clip.
            SpecialSfxSource2.Play();
            FirstSpSource = true;
        }
    }
    public void PlaySingle(AudioClip clip, bool RandPitch, bool TimeScalePitch)
    {
        //Set the clip of our efxSource audio source to the clip passed in as a parameter.

        if (RandPitch)
        {
            if (TimeScalePitch) randomPitch = Random.Range(lowPitchRange, highPitchRange) * Time.timeScale;
            else randomPitch = Random.Range(lowPitchRange, highPitchRange);
        }
        else
        {
            if (TimeScalePitch) randomPitch = Time.timeScale;
            else randomPitch = 1;
        }

        if (FirstSource)
        {
            sfxSource.clip = clip;//Set the pitch of the audio source to the randomly chosen pitch.
            sfxSource.pitch = randomPitch;
            //Play the clip.
            sfxSource.Play();
            FirstSource = false;
        }
        else
        {
            sfxSource2.clip = clip;
            sfxSource2.pitch = randomPitch;
            //Play the clip.
            sfxSource2.Play();
            FirstSource = true;
        }
    }

    float randomPitch = 1;
    //RandomizeSfx chooses randomly between various audio clips and slightly changes their pitch.
    public void RandomizeSfx(params AudioClip[] clips)
    {
        //Generate a random number between 0 and the length of our array of clips passed in.
        int randomIndex = Random.Range(0, clips.Length);

        //Choose a random pitch to play back our clip at between our high and low pitch ranges.
        if (SlowmoSfx) randomPitch = Random.Range(lowPitchRange, highPitchRange) * Time.timeScale;
        else randomPitch = Random.Range(lowPitchRange, highPitchRange);

        //Set the pitch of the audio source to the randomly chosen pitch.
        RandomSfxSource.pitch = randomPitch;

        //Set the clip to the clip at our randomly chosen index.
        RandomSfxSource.clip = clips[randomIndex];

        //Play the clip.
        RandomSfxSource.Play();
    }
    void Update()
    {
        if (debugtime) Time.timeScale = scale;
        if (SlowmoMusic) musicSource.pitch = Time.timeScale;
    }
    float v;
    public void Fadeout() { StartCoroutine(fader(musicSource.volume, 0, 1)); }
    public void FadeTo(float targetVolume, float time) { StartCoroutine(fader(musicSource.volume, targetVolume, time)); }
    IEnumerator fader(float source, float target, float t)
    {
        float startTime = Time.time;
        while (Time.time < startTime + t)
        {
            musicSource.volume = Mathf.Lerp(musicSource.volume, target, (Time.time-startTime)/(t));
            yield return null;
        }
        if(Time.time > startTime + t) musicSource.volume = target;
    }
}
