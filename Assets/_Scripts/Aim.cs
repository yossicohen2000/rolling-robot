﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Aim : MonoBehaviour {
Transform myTrans;
public float Sensitivity=2;
float zoom;
public Transform Target,Crosshair;
	// Use this for initialization
		Vector3 v=Vector3.zero;
		public Vector3 offset;
	void Start () {
		myTrans=transform;
		zoom=offset.z;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		RaycastHit hit;
		if(Physics.Raycast(myTrans.position,myTrans.forward,out hit))
		Crosshair.position=hit.point;
		else Crosshair.position=myTrans.position+myTrans.forward*100;

		if(Physics.Raycast(Target.position-myTrans.forward,-myTrans.forward,out hit))
		if(hit.distance<-zoom-1)offset.z=-hit.distance-1;
		else
		offset.z=zoom;
		
	}

    public float DampTime=0.5f;

    private void LateUpdate() {
		myTrans.position=Vector3.SmoothDamp(myTrans.position,Target.position+myTrans.forward*offset.z+Vector3.up*offset.y+myTrans.right*offset.x,ref v,DampTime);
		myTrans.RotateAround(Target.position,Vector3.up,Input.GetAxis("Mouse X")*Sensitivity*Time.deltaTime);
		myTrans.RotateAround(Target.position,myTrans.right,-Input.GetAxis("Mouse Y")*Sensitivity*Time.deltaTime);
	}
}
